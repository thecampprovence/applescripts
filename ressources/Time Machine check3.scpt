FasdUAS 1.101.10   ��   ��    k             l     ��������  ��  ��        l     �� 	 
��   	 = 7 Applescript to monitor age of last time machine backup    
 �   n   A p p l e s c r i p t   t o   m o n i t o r   a g e   o f   l a s t   t i m e   m a c h i n e   b a c k u p      l     ��  ��    7 1 and send alert to notification center if too old     �   b   a n d   s e n d   a l e r t   t o   n o t i f i c a t i o n   c e n t e r   i f   t o o   o l d      l     ��������  ��  ��        l     ��  ��    ( " set sendTo to "julien@thecamp.fr"     �   D   s e t   s e n d T o   t o   " j u l i e n @ t h e c a m p . f r "      l     ��������  ��  ��        l     ����  r         m     ��
�� boovfals  o      ���� $0 shownotification showNotification��  ��         l    !���� ! r     " # " m     $ $ � % % . S a u v e g a r d e   T i m e   M a c h i n e # o      ���� &0 notificationtitle notificationTitle��  ��      & ' & l    (���� ( r     ) * ) m    	 + + � , ,   * o      ���� 0 tmlatest tmLatest��  ��   '  - . - l     ��������  ��  ��   .  / 0 / l   - 1���� 1 Q    - 2 3 4 2 r     5 6 5 I   �� 7��
�� .sysoexecTEXT���     TEXT 7 m     8 8 � 9 9 8 / u s r / b i n / t m u t i l   l a t e s t b a c k u p��   6 o      ���� 0 tmlatest tmLatest 3 R      �� : ;
�� .ascrerr ****      � **** : o      ���� 0 emsg eMsg ; �� <��
�� 
errn < o      ���� 0 enum eNum��   4 k    - = =  > ? > r    # @ A @ b    ! B C B m     D D � E E t I m p o s s i b l e   d ' a c c � d e r   �   l a   d e r n i � r e   s a u v e g a r d e   !      E r r e u r :   C o     ���� 0 emsg eMsg A o      ���� *0 notificationmessage notificationMessage ?  F G F r   $ ' H I H m   $ %��
�� boovtrue I o      ���� $0 shownotification showNotification G  J K J l  ( (��������  ��  ��   K  L�� L I  ( -�� M��
�� .ascrcmnt****      � **** M m   ( ) N N � O O $ N o   T M   b a c k u p   f o u n d��  ��  ��  ��   0  P Q P l     ��������  ��  ��   Q  R S R l  . � T���� T Z   . � U V���� U E   . 1 W X W o   . /���� 0 tmlatest tmLatest X m   / 0 Y Y � Z Z   B a c k u p s . b a c k u p d b V k   4 � [ [  \ ] \ r   4 : ^ _ ^ 4   4 8�� `
�� 
psxf ` o   6 7���� 0 tmlatest tmLatest _ o      ���� 
0 latest   ]  a b a r   ; H c d c n   ; D e f e 1   @ D��
�� 
asmo f l  ; @ g���� g I  ; @�� h��
�� .sysonfo4asfe        file h o   ; <���� 
0 latest  ��  ��  ��   d o      ���� 0 mod_date   b  i j i r   I ^ k l k I  I Z�� m��
�� .sysorondlong        doub m l  I V n���� n ^   I V o p o l  I R q���� q \   I R r s r l  I N t���� t I  I N������
�� .misccurdldt    ��� null��  ��  ��  ��   s o   N Q���� 0 mod_date  ��  ��   p 1   R U��
�� 
days��  ��  ��   l o      ���� 0 file_age   j  u v u l  _ _��������  ��  ��   v  w x w I  _ n�� y��
�� .ascrcmnt****      � **** y b   _ j z { z b   _ f | } | m   _ b ~ ~ �   , m o s t   r e c e n t   T M   b a c k u p   } o   b e���� 0 file_age   { m   f i � � � � �    d a y s   o l d��   x  � � � l  o o��������  ��  ��   �  ��� � Z   o � � ����� � ?   o t � � � o   o r���� 0 file_age   � m   r s����  � k   w � � �  � � � r   w | � � � m   w z � � � � � f A t t e n t i o n   !   V o t r e   d e r n i � r e   s a u v e g a r d e   e s t   a n c i e n n e . � o      ���� *0 notificationmessage notificationMessage �  � � � r   } � � � � m   } ~��
�� boovtrue � o      ���� $0 shownotification showNotification �  � � � l  � ���������  ��  ��   �  ��� � l   � ��� � ���   �\Vset cn to computer name of (system info)		tell application "Mail"			tell (make new outgoing message)				set subject to cn & " TM Backup Fail"				set content to "Most recent " & cn & " TM backup is " & file_age & " days old"				make new to recipient at end of to recipients with properties {address:sendTo}				send			end tell		end tell    � � � �� s e t   c n   t o   c o m p u t e r   n a m e   o f   ( s y s t e m   i n f o )  	 	 t e l l   a p p l i c a t i o n   " M a i l "  	 	 	 t e l l   ( m a k e   n e w   o u t g o i n g   m e s s a g e )  	 	 	 	 s e t   s u b j e c t   t o   c n   &   "   T M   B a c k u p   F a i l "  	 	 	 	 s e t   c o n t e n t   t o   " M o s t   r e c e n t   "   &   c n   &   "   T M   b a c k u p   i s   "   &   f i l e _ a g e   &   "   d a y s   o l d "  	 	 	 	 m a k e   n e w   t o   r e c i p i e n t   a t   e n d   o f   t o   r e c i p i e n t s   w i t h   p r o p e r t i e s   { a d d r e s s : s e n d T o }  	 	 	 	 s e n d  	 	 	 e n d   t e l l  	 	 e n d   t e l l��  ��  ��  ��  ��  ��  ��  ��   S  � � � l     ��������  ��  ��   �  ��� � l  � � ����� � Z   � � � ����� � =  � � � � � o   � ����� $0 shownotification showNotification � m   � ���
�� boovtrue � k   � � � �  � � � I  � ��� � �
�� .sysonotfnull��� ��� TEXT � o   � ����� *0 notificationmessage notificationMessage � �� � �
�� 
appr � o   � ����� &0 notificationtitle notificationTitle � �� ���
�� 
nsou � m   � � � � � � � 
 B a s s o��   �  ��� � I  � ��� ���
�� .sysodelanull��� ��� nmbr � m   � ����� ��  ��  ��  ��  ��  ��  ��       �� � ���   � ��
�� .aevtoappnull  �   � **** � �� ����� � ���
�� .aevtoappnull  �   � **** � k     � � �   � �   � �  & � �  / � �  R � �  �����  ��  ��   � ����� 0 emsg eMsg� 0 enum eNum �  �~ $�} +�| 8�{�z � D�y N�x Y�w�v�u�t�s�r�q�p�o ~ � ��n�m ��l�k�j�~ $0 shownotification showNotification�} &0 notificationtitle notificationTitle�| 0 tmlatest tmLatest
�{ .sysoexecTEXT���     TEXT�z 0 emsg eMsg � �i�h�g
�i 
errn�h 0 enum eNum�g  �y *0 notificationmessage notificationMessage
�x .ascrcmnt****      � ****
�w 
psxf�v 
0 latest  
�u .sysonfo4asfe        file
�t 
asmo�s 0 mod_date  
�r .misccurdldt    ��� null
�q 
days
�p .sysorondlong        doub�o 0 file_age  
�n 
appr
�m 
nsou�l 
�k .sysonotfnull��� ��� TEXT
�j .sysodelanull��� ��� nmbr�� �fE�O�E�O�E�O �j E�W X  �%E�OeE�O�j O�� W*��/E�O�j a ,E` O*j _ _ !j E` Oa _ %a %j O_ m a E�OeE�OPY hY hO�e  �a �a a a  Okj Y hascr  ��ޭ